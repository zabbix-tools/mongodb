/*
** Zabbix
** Copyright 2001-2023 Zabbix SIA
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**/

package plugin

import (
	"git.zabbix.com/ap/mongodb/plugin/handlers"
	"git.zabbix.com/ap/plugin-support/metric"
	"git.zabbix.com/ap/plugin-support/plugin"
	"git.zabbix.com/ap/plugin-support/uri"
)

// handlerFunc defines an interface must be implemented by handlers.
type handlerFunc func(s handlers.Session, params map[string]string) (res interface{}, err error)

var metricHandlers = map[string]handlerFunc{
	keyConfigDiscovery:      handlers.ConfigDiscoveryHandler,
	keyCollectionStats:      handlers.CollectionStatsHandler,
	keyCollectionsDiscovery: handlers.CollectionsDiscoveryHandler,
	keyCollectionsUsage:     handlers.CollectionsUsageHandler,
	keyConnPoolStats:        handlers.ConnPoolStatsHandler,
	keyDatabaseStats:        handlers.DatabaseStatsHandler,
	keyDatabasesDiscovery:   handlers.DatabasesDiscoveryHandler,
	keyJumboChunks:          handlers.JumboChunksHandler,
	keyOplogStats:           handlers.OplogStatsHandler,
	keyPing:                 handlers.PingHandler,
	keyReplSetConfig:        handlers.ReplSetConfigHandler,
	keyReplSetStatus:        handlers.ReplSetStatusHandler,
	keyServerStatus:         handlers.ServerStatusHandler,
	keyShardsDiscovery:      handlers.ShardsDiscoveryHandler,
}

// getHandlerFunc returns a handlerFunc related to a given key.
func getHandlerFunc(key string) handlerFunc {
	return metricHandlers[key]
}

const (
	keyConfigDiscovery      = "mongodb.cfg.discovery"
	keyCollectionStats      = "mongodb.collection.stats"
	keyCollectionsDiscovery = "mongodb.collections.discovery"
	keyCollectionsUsage     = "mongodb.collections.usage"
	keyConnPoolStats        = "mongodb.connpool.stats"
	keyDatabaseStats        = "mongodb.db.stats"
	keyDatabasesDiscovery   = "mongodb.db.discovery"
	keyJumboChunks          = "mongodb.jumbo_chunks.count"
	keyOplogStats           = "mongodb.oplog.stats"
	keyPing                 = "mongodb.ping"
	keyReplSetConfig        = "mongodb.rs.config"
	keyReplSetStatus        = "mongodb.rs.status"
	keyServerStatus         = "mongodb.server.status"
	keyShardsDiscovery      = "mongodb.sh.discovery"
)

var (
	paramURI = metric.NewConnParam("URI", "URI to connect or session name.").
			WithDefault(handlers.UriDefaults.Scheme + "://localhost:" + handlers.UriDefaults.Port).WithSession().
			WithValidator(uri.URIValidator{Defaults: handlers.UriDefaults, AllowedSchemes: []string{"tcp"}})
	paramUser        = metric.NewConnParam("User", "MongoDB user.")
	paramPassword    = metric.NewConnParam("Password", "User's password.")
	paramDatabase    = metric.NewParam("Database", "Database name.").WithDefault("admin")
	paramCollection  = metric.NewParam("Collection", "Collection name.").SetRequired()
	paramTLSConnect  = metric.NewSessionOnlyParam("TLSConnect", "DB connection encryption type.").WithDefault("")
	paramTLSCaFile   = metric.NewSessionOnlyParam("TLSCAFile", "TLS ca file path.").WithDefault("")
	paramTLSCertFile = metric.NewSessionOnlyParam("TLSCertFile", "TLS cert file path.").WithDefault("")
	paramTLSKeyFile  = metric.NewSessionOnlyParam("TLSKeyFile", "TLS key file path.").WithDefault("")
)

var metrics = metric.MetricSet{
	keyConfigDiscovery: metric.New("Returns a list of discovered config servers.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword, paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyCollectionStats: metric.New("Returns a variety of storage statistics for a given collection.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword, paramDatabase, paramCollection,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyCollectionsDiscovery: metric.New("Returns a list of discovered collections.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyCollectionsUsage: metric.New("Returns usage statistics for collections.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyConnPoolStats: metric.New("Returns information regarding the open outgoing connections from the "+
		"current database instance to other members of the sharded cluster or replica set.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyDatabaseStats: metric.New("Returns statistics reflecting a given database system’s state.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword, paramDatabase,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyDatabasesDiscovery: metric.New("Returns a list of discovered databases.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyJumboChunks: metric.New("Returns count of jumbo chunks.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyOplogStats: metric.New("Returns a status of the replica set, using data polled from the oplog.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyPing: metric.New("Test if connection is alive or not.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyReplSetConfig: metric.New("Returns a current configuration of the replica set.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyReplSetStatus: metric.New("Returns a replica set status from the point of view of the member "+
		"where the method is run.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyServerStatus: metric.New("Returns a database’s state.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),

	keyShardsDiscovery: metric.New("Returns a list of discovered shards present in the cluster.",
		[]*metric.Param{
			paramURI, paramUser, paramPassword,
			paramTLSConnect, paramTLSCaFile, paramTLSCertFile, paramTLSKeyFile,
		}, false),
}

func init() {
	plugin.RegisterMetrics(&Impl, Name, metrics.List()...)
}
